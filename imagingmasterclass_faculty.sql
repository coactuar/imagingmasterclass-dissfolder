-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 10:51 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imagingmasterclass_faculty`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(50) NOT NULL,
  `Full Name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `city` varchar(150) NOT NULL,
  `hospital` varchar(200) NOT NULL,
  `UserAction` varchar(200) NOT NULL,
  `eventname` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `Full Name`, `email`, `city`, `hospital`, `UserAction`, `eventname`) VALUES
(1, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'Av', '25-02-2021 19:10', '25-02-2021 21:47'),
(2, 'Muhammed Nizar', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', '26-02-2021 08:15', '26-02-2021 21:47'),
(3, 'Jaspreet', 'jaspreet.singhwalia@abbott.com', 'New Delhi', 'Abbott', '26-02-2021 19:14', '26-02-2021 19:20'),
(4, 'Jasptreet', 'jaspreet.singhwalia@abbott.com', 'Delhi', 'Abbott', '26-02-2021 19:21', '26-02-2021 21:47'),
(5, 'Kevin Croce', 'kcroce@partners.org', 'Boston', 'BWH', '26-02-2021 18:26', '26-02-2021 18:27'),
(6, 'Jebin John', 'jebin.john229@jtb-india.com', 'Bengaluru', 'JTB India Pvt. Ltd.', '26-02-2021 19:00', '26-02-2021 21:47'),
(7, 'Dr P K Sahoo', 'drprasant_s@apollohospitals.com', 'Bhubaneswar', 'Apollo Hospital', '26-02-2021 19:46', '26-02-2021 21:47'),
(8, 'Uday Khanolkar', 'udaykhanolkar2002@yahoo.co.in', 'Bangalore', 'Narayana Hrudayalaya', '26-02-2021 19:46', '26-02-2021 21:42'),
(9, 'Tom Devasia', 'tomdevasia@hotmail.com', 'Manipal', 'KMC Manipal', '26-02-2021 19:46', '26-02-2021 19:46'),
(10, 'Tom Devasia', 'tomdevasia@hotmail.com', 'Manipal', 'KMC Manipal', '26-02-2021 19:47', '26-02-2021 21:47'),
(11, 'Dr Rony Mathew', 'drronymathew@yahoo.com', 'Kochi', 'Lisie Hospital', '26-02-2021 19:48', '26-02-2021 19:49'),
(12, 'Kevin Croce', 'kcroce@bwh.harvard.edu', 'Boston', 'Brigham and Women\'s / Harvard Med', '26-02-2021 19:49', '26-02-2021 21:47'),
(13, 'Dr AJ Swamy', 'ajayswamy@rediffmail.com', 'Bangalore', 'Command Hospital', '26-02-2021 19:50', '26-02-2021 20:09'),
(14, 'Kevin James Croce', 'kcroce@bwh.harvard.edu', 'Boston', 'Brigham and Women\'s / Harvard Med', '26-02-2021 19:50', '26-02-2021 19:51'),
(15, 'Dr D S Chadha', 'agiamu@gmail.com', 'BANGALORE', 'Manipal Hospital ', '26-02-2021 19:59', '26-02-2021 21:30'),
(16, 'Dr Rony Mathew', 'drronymathew@yahoo.com', 'Kochi', 'Lisie Hospital', '26-02-2021 19:50', '26-02-2021 21:17'),
(17, 'Dr Girish B Navasundi', 'navasundi@rediffmail.com', 'Bangalore', 'Apollo Hospitals Bangalore', '26-02-2021 20:04', '26-02-2021 20:19'),
(18, 'DR SREENIVAS KUMAR', 'careask@gmail.com', 'hyd', 'Apollo', '26-02-2021 20:03', '26-02-2021 20:10'),
(19, 'Dr Girish B Navasundi', 'navasundi@rediffmail.com', 'Bangalore', 'Apollo Hospital', '26-02-2021 20:22', '26-02-2021 21:00'),
(20, 'Dr aj swamy', 'ajayswamy@rediffmail.com', 'bangalore', 'command hospital', '26-02-2021 20:10', '26-02-2021 21:47'),
(21, 'Dr Girish B Navasundi', 'navasundi@rediffmail.com', 'Bangalore', 'Apollo Hospitals', '26-02-2021 21:02', '26-02-2021 21:47'),
(22, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', '26-02-2021 20:01', '26-02-2021 21:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'akshat', 'neeraj@coact.co.in', 'neeraj', '9700467764', NULL, NULL, '2021-02-25 11:15:51', '2021-02-25 11:15:51', '2021-02-25 12:33:54', 0, 'Abbott'),
(2, 'akshat', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 12:26:35', '2021-02-25 12:26:35', '2021-02-25 12:33:54', 0, 'Abbott'),
(3, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 12:30:00', '2021-02-25 12:30:00', '2021-02-25 12:33:54', 0, 'Abbott'),
(4, 'akshat', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 12:30:43', '2021-02-25 12:30:43', '2021-02-25 12:33:54', 0, 'Abbott'),
(5, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 12:30:52', '2021-02-25 12:30:52', '2021-02-25 12:33:54', 0, 'Abbott'),
(6, 'akshat', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 12:32:22', '2021-02-25 12:32:22', '2021-02-25 12:33:54', 0, 'Abbott'),
(7, 'akshat', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 12:33:37', '2021-02-25 12:33:37', '2021-02-25 12:33:54', 0, 'Abbott'),
(8, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 12:33:49', '2021-02-25 12:33:49', '2021-02-25 12:33:54', 0, 'Abbott'),
(9, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 12:33:59', '2021-02-25 12:33:59', '2021-02-25 12:34:29', 1, 'Abbott'),
(10, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'Av', NULL, NULL, '2021-02-25 13:31:48', '2021-02-25 13:31:48', '2021-02-25 13:32:18', 1, 'Abbott'),
(11, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-02-25 13:31:57', '2021-02-25 13:31:57', '2021-02-25 21:16:50', 1, 'Abbott'),
(12, 'akshat1', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 14:30:37', '2021-02-25 14:30:37', '2021-02-25 14:31:07', 1, 'Abbott'),
(13, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-02-25 14:32:28', '2021-02-25 14:32:28', '2021-02-25 21:16:50', 1, 'Abbott'),
(14, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-02-25 14:51:12', '2021-02-25 14:51:12', '2021-02-25 21:16:50', 1, 'Abbott'),
(15, 'Muhammed Nizar', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-02-26 08:15:55', '2021-02-26 08:15:55', '2021-02-26 08:16:25', 1, 'Abbott'),
(16, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-02-26 14:58:50', '2021-02-26 14:58:50', '2021-02-26 14:59:20', 1, 'Abbott'),
(17, 'Jaspreet', 'jaspreet.singhwalia@abbott.com', 'New Delhi', 'Abbott', NULL, NULL, '2021-02-26 15:07:05', '2021-02-26 15:07:05', '2021-02-26 15:07:35', 1, 'Abbott'),
(18, 'Jasptreet', 'jaspreet.singhwalia@abbott.com', 'Delhi', 'Abbott', NULL, NULL, '2021-02-26 15:12:47', '2021-02-26 15:12:47', '2021-02-26 15:13:17', 1, 'Abbott'),
(19, 'Kevin Croce', 'kcroce@partners.org', 'Boston', 'BWH', NULL, NULL, '2021-02-26 18:26:52', '2021-02-26 18:26:52', '2021-02-26 18:27:22', 1, 'Abbott'),
(20, 'Jebin John', 'jebin.john229@jtb-india.com', 'Bengaluru', 'JTB India Pvt. Ltd.', NULL, NULL, '2021-02-26 19:00:49', '2021-02-26 19:00:49', '2021-02-26 19:01:19', 1, 'Abbott'),
(21, 'Muhammed Nizar', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-02-26 19:02:51', '2021-02-26 19:02:51', '2021-02-26 19:03:21', 1, 'Abbott'),
(22, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-02-26 19:09:38', '2021-02-26 19:09:38', '2021-02-26 19:10:08', 1, 'Abbott'),
(23, 'Jasptreet', 'jaspreet.singhwalia@abbott.com', 'Delhi', 'Abbott', NULL, NULL, '2021-02-26 19:14:16', '2021-02-26 19:14:16', '2021-02-26 19:14:46', 1, 'Abbott'),
(24, 'Jasptreet', 'jaspreet.singhwalia@abbott.com', 'Delhi', 'Abbott', NULL, NULL, '2021-02-26 19:21:00', '2021-02-26 19:21:00', '2021-02-26 19:21:30', 1, 'Abbott'),
(25, 'Dr P K Sahoo', 'drprasant_s@apollohospitals.com', 'Bhubaneswar', 'Apollo Hospital', NULL, NULL, '2021-02-26 19:44:25', '2021-02-26 19:44:25', '2021-02-26 19:44:55', 1, 'Abbott'),
(26, 'Uday Khanolkar', 'udaykhanolkar2002@yahoo.co.in', 'Bangalore', 'Narayana Hrudayalaya', NULL, NULL, '2021-02-26 19:45:08', '2021-02-26 19:45:08', '2021-02-26 19:45:38', 1, 'Abbott'),
(27, 'Tom Devasia', 'tomdevasia@hotmail.com', 'Manipal', 'KMC Manipal', NULL, NULL, '2021-02-26 19:45:56', '2021-02-26 19:45:56', '2021-02-26 19:46:26', 1, 'Abbott'),
(28, 'Tom Devasia', 'tomdevasia@hotmail.com', 'Manipal', 'KMC Manipal', NULL, NULL, '2021-02-26 19:46:17', '2021-02-26 19:46:17', '2021-02-26 19:46:47', 1, 'Abbott'),
(29, 'Tom Devasia', 'tomdevasia@hotmail.com', 'Manipal', 'KMC Manipal', NULL, NULL, '2021-02-26 19:46:57', '2021-02-26 19:46:57', '2021-02-26 19:47:27', 1, 'Abbott'),
(30, 'Dr Rony Mathew', 'drronymathew@yahoo.com', 'Kochi', 'Lisie Hospital', NULL, NULL, '2021-02-26 19:48:32', '2021-02-26 19:48:32', '2021-02-26 19:49:02', 1, 'Abbott'),
(31, 'Kevin Croce', 'kcroce@bwh.harvard.edu', 'Boston', 'Brigham and Women\'s / Harvard Med', NULL, NULL, '2021-02-26 19:48:44', '2021-02-26 19:48:44', '2021-02-26 19:49:14', 1, 'Abbott'),
(32, 'Dr AJ Swamy', 'ajayswamy@rediffmail.com', 'Bangalore', 'Command Hospital', NULL, NULL, '2021-02-26 19:49:55', '2021-02-26 19:49:55', '2021-02-26 19:50:25', 1, 'Abbott'),
(33, 'Kevin James Croce', 'kcroce@bwh.harvard.edu', 'Boston', 'Brigham and Women\'s / Harvard Med', NULL, NULL, '2021-02-26 19:50:30', '2021-02-26 19:50:30', '2021-02-26 19:51:00', 1, 'Abbott'),
(34, 'Dr D S Chadha', 'agiamu@gmail.com', 'BANGALORE', 'Manipal Hospital ', NULL, NULL, '2021-02-26 19:53:29', '2021-02-26 19:53:29', '2021-02-26 19:53:59', 1, 'Abbott'),
(35, 'Dr Rony Mathew', 'drronymathew@yahoo.com', 'Kochi', 'Lisie Hospital', NULL, NULL, '2021-02-26 19:54:27', '2021-02-26 19:54:27', '2021-02-26 19:54:57', 1, 'Abbott'),
(36, 'Dr Girish B Navasundi', 'navasundi@rediffmail.com', 'Bangalore', 'Apollo Hospitals Bangalore', NULL, NULL, '2021-02-26 19:56:27', '2021-02-26 19:56:27', '2021-02-26 19:56:57', 1, 'Abbott'),
(37, 'DR SREENIVAS KUMAR', 'careask@gmail.com', 'hyd', 'Apollo', NULL, NULL, '2021-02-26 20:00:09', '2021-02-26 20:00:09', '2021-02-26 20:00:39', 1, 'Abbott'),
(38, 'Dr Girish B Navasundi', 'navasundi@rediffmail.com', 'Bangalore', 'Apollo Hospital', NULL, NULL, '2021-02-26 20:04:01', '2021-02-26 20:04:01', '2021-02-26 20:04:31', 1, 'Abbott'),
(39, 'Dr aj swamy', 'ajayswamy@rediffmail.com', 'bangalore', 'command hospital', NULL, NULL, '2021-02-26 20:10:04', '2021-02-26 20:10:04', '2021-02-26 20:10:34', 1, 'Abbott'),
(40, 'Kevin Croce', 'kcroce@partners.org', 'Boston', 'Brigham and Women\'s / Harvard Med', NULL, NULL, '2021-02-26 20:38:20', '2021-02-26 20:38:20', '2021-02-26 20:38:50', 1, 'Abbott'),
(41, 'Dr Girish B Navasundi', 'navasundi@rediffmail.com', 'Bangalore', 'Apollo Hospitals', NULL, NULL, '2021-02-26 21:02:05', '2021-02-26 21:02:05', '2021-02-26 21:02:35', 1, 'Abbott'),
(42, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-02-26 21:21:56', '2021-02-26 21:21:56', '2021-02-26 21:22:26', 1, 'Abbott'),
(43, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-02-26 21:22:58', '2021-02-26 21:22:58', '2021-02-26 21:23:28', 1, 'Abbott');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
