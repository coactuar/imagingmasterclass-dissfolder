-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 10:51 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imagingmasterclass`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'akshat', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 11:50:40', '2021-02-25 11:50:40', '2021-02-25 20:56:52', 0, 'Abbott'),
(2, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 12:34:14', '2021-02-25 12:34:14', '2021-02-25 20:56:52', 0, 'Abbott'),
(3, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'BAngalore', NULL, NULL, '2021-02-25 14:57:48', '2021-02-25 14:57:48', '2021-03-02 13:15:08', 0, 'Abbott'),
(4, 'Kalyan', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'Av', NULL, NULL, '2021-02-25 19:51:46', '2021-02-25 19:51:46', '2021-03-01 16:03:42', 0, 'Abbott'),
(5, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-02-25 19:57:26', '2021-02-25 19:57:26', '2021-02-26 21:47:58', 0, 'Abbott'),
(6, 'akshat', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-02-25 20:56:48', '2021-02-25 20:56:48', '2021-02-25 20:56:52', 0, 'Abbott'),
(7, 'M W APP ', 'mw.app81@gmail.com', 'Mumbai ', 'M W HOSPITAL ', NULL, NULL, '2021-02-26 09:38:39', '2021-02-26 09:38:39', '2021-02-27 16:57:04', 0, 'Abbott'),
(8, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-02-26 11:42:48', '2021-02-26 11:42:48', '2021-02-26 21:18:09', 0, 'Abbott'),
(9, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-02-26 12:09:17', '2021-02-26 12:09:17', '2021-02-26 12:31:11', 0, 'Abbott'),
(10, 'Madakari Nayaka', '2020madhu.icare@gmail.com', 'Bengaluru ', 'SPARSH', NULL, NULL, '2021-02-26 14:13:38', '2021-02-26 14:13:38', '2021-02-26 14:19:45', 0, 'Abbott'),
(11, 'Amal', 'amalrosh38@gmail.com', 'Bangalore ', 'Manipal hospital ', NULL, NULL, '2021-02-26 19:23:11', '2021-02-26 19:23:11', '2021-02-26 21:05:59', 0, 'Abbott'),
(12, 'Amal', 'amalrosh38@gmail.com', 'Bangalore ', 'Manipal hospital ', NULL, NULL, '2021-02-26 19:23:31', '2021-02-26 19:23:31', '2021-02-26 21:05:59', 0, 'Abbott'),
(13, 'Jebin John', 'jebin.john229@jtb-india.com', 'Bangalore', 'JTB India Pvt. Ltd.', NULL, NULL, '2021-02-26 19:25:27', '2021-02-26 19:25:27', '2021-02-26 19:29:28', 0, 'Abbott'),
(14, 'Mahendra Kumar G', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-02-26 19:42:48', '2021-02-26 19:42:48', '2021-02-26 21:47:44', 0, 'Abbott'),
(15, 'Jagadesh Kalathil', 'jagadeesh.kalathil@abbott.com', 'Bangalore East', 'AV', NULL, NULL, '2021-02-26 19:50:05', '2021-02-26 19:50:05', '2021-02-26 21:35:06', 0, 'Abbott'),
(16, 'Nuthan U', 'nuthanmohite@gmail.com', 'Bannerghatta road. Bangalore', 'Apollo', NULL, NULL, '2021-02-26 19:51:36', '2021-02-26 19:51:36', '2021-02-26 19:57:08', 0, 'Abbott'),
(17, 'Pooja', 'pooja@coact.co.in', 'Mumbai ', 'Test', NULL, NULL, '2021-02-26 19:55:56', '2021-02-26 19:55:56', '2021-02-26 20:25:03', 0, 'Abbott'),
(18, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Bangalore Baptist hospital', NULL, NULL, '2021-02-26 19:56:24', '2021-02-26 19:56:24', '2021-02-26 21:08:26', 0, 'Abbott'),
(19, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Bangalore Baptist hospital', NULL, NULL, '2021-02-26 19:56:27', '2021-02-26 19:56:27', '2021-02-26 21:08:26', 0, 'Abbott'),
(20, 'Arun Jame', 'arun.nelluvelil@abbott.com', 'BANGALORE', 'Av', NULL, NULL, '2021-02-26 20:00:35', '2021-02-26 20:00:35', '2021-02-26 21:45:44', 0, 'Abbott'),
(21, 'Amal', 'amalrosh38@gmail.com', 'Bangalore ', 'Manipal', NULL, NULL, '2021-02-26 20:03:09', '2021-02-26 20:03:09', '2021-02-26 21:05:59', 0, 'Abbott'),
(22, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Bangalore Baptist hospital', NULL, NULL, '2021-02-26 20:03:15', '2021-02-26 20:03:15', '2021-02-26 21:08:26', 0, 'Abbott'),
(23, 'Abhilash ', 'abhilash.mohanan@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-02-26 20:03:57', '2021-02-26 20:03:57', '2021-02-26 21:55:04', 0, 'Abbott'),
(24, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Bangalore Baptist hospital', NULL, NULL, '2021-02-26 20:04:09', '2021-02-26 20:04:09', '2021-02-26 21:08:26', 0, 'Abbott'),
(25, 'Sojo ', 'sojo.joy@abbott.com', 'Vazhakilam', 'Abbott', NULL, NULL, '2021-02-26 20:05:01', '2021-02-26 20:05:01', '2021-02-26 21:15:51', 0, 'Abbott'),
(26, 'sathya narayana', 'drsathyanh@gmail.com', 'BANGALORE', 'NARAYANA HRUDALAYA', NULL, NULL, '2021-02-26 20:05:02', '2021-02-26 20:05:02', '2021-02-26 21:17:34', 0, 'Abbott'),
(27, 'Shouvik Bhattacharya', 'shouvik.bhattacharya@abbott.com', 'Kolkata', 'Abbott Vascular ', NULL, NULL, '2021-02-26 20:05:16', '2021-02-26 20:05:16', '2021-02-26 21:07:36', 0, 'Abbott'),
(28, 'Sreevatsa N S', 'nadig33@gmail.com', 'Shimoga', 'Shimoga', NULL, NULL, '2021-02-26 20:05:43', '2021-02-26 20:05:43', '2021-02-26 20:08:13', 0, 'Abbott'),
(29, 'Amal', 'amalrosh38@gmail.com', 'Bangalore ', 'Manipa', NULL, NULL, '2021-02-26 20:05:58', '2021-02-26 20:05:58', '2021-02-26 21:05:59', 0, 'Abbott'),
(30, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-02-26 20:06:14', '2021-02-26 20:06:14', '2021-02-26 20:12:44', 0, 'Abbott'),
(31, 'dr sarat kumae sahoo', 'drsaratsahoo@rediffmail.com', 'bhubaneswar', 'sumum', NULL, NULL, '2021-02-26 20:06:19', '2021-02-26 20:06:19', '2021-02-26 21:42:49', 0, 'Abbott'),
(32, 'Kethan Chowdary Galla', 'galla.kethan@gmail.com', 'Bangalore', 'Apollo hospital', NULL, NULL, '2021-02-26 20:06:37', '2021-02-26 20:06:37', '2021-02-26 20:34:49', 0, 'Abbott'),
(33, 'ANKIT SINGH ', 'ankitsingh0612@gmail.com', 'Bengaluru', 'Sri jayadeva institute of cardiovascular sciences and research ', NULL, NULL, '2021-02-26 20:06:40', '2021-02-26 20:06:40', '2021-02-26 21:28:41', 0, 'Abbott'),
(34, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021-02-26 20:06:41', '2021-02-26 20:06:41', '2021-02-26 22:02:30', 0, 'Abbott'),
(35, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-02-26 20:06:54', '2021-02-26 20:06:54', '2021-03-01 16:03:42', 0, 'Abbott'),
(36, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Baptist hospital', NULL, NULL, '2021-02-26 20:07:05', '2021-02-26 20:07:05', '2021-02-26 21:08:26', 0, 'Abbott'),
(37, 'Mahendra Kumar G', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-02-26 20:07:14', '2021-02-26 20:07:14', '2021-02-26 21:47:44', 0, 'Abbott'),
(38, 'Reeshma', 'reeshmadvdg@gmail.com', 'Manipal', 'KH hospital', NULL, NULL, '2021-02-26 20:08:43', '2021-02-26 20:08:43', '2021-02-26 20:26:53', 0, 'Abbott'),
(39, 'Vijay Shah', 'drvtshah19@gmail.com', 'Mumbai ', 'Dr v t shah clinic ', NULL, NULL, '2021-02-26 20:09:20', '2021-02-26 20:09:20', '2021-02-26 20:30:22', 0, 'Abbott'),
(40, 'Rajagopal ', 'rajagopalj@gmail.com', 'Mysore', 'Cauvery heart hosp ', NULL, NULL, '2021-02-26 20:09:22', '2021-02-26 20:09:22', '2021-02-26 20:16:22', 0, 'Abbott'),
(41, 'Rajeev Chauhan', 'rajeevkilroy@yahoo.com', 'Bengaluru', 'Command', NULL, NULL, '2021-02-26 20:09:33', '2021-02-26 20:09:33', '2021-02-26 20:38:04', 0, 'Abbott'),
(42, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-02-26 20:10:01', '2021-02-26 20:10:01', '2021-02-26 20:43:47', 0, 'Abbott'),
(43, 'Nithin', 'nithinrao972@gmail.com', 'Bangalore', 'Sri jayadeva ', NULL, NULL, '2021-02-26 20:10:36', '2021-02-26 20:10:36', '2021-02-26 20:19:52', 0, 'Abbott'),
(44, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV', NULL, NULL, '2021-02-26 20:11:02', '2021-02-26 20:11:02', '2021-03-01 16:03:42', 0, 'Abbott'),
(45, 'Keshava ', 'drkeshavar@gmail.com', 'Bangalore ', 'Fortis ', NULL, NULL, '2021-02-26 20:11:25', '2021-02-26 20:11:25', '2021-02-26 21:09:25', 0, 'Abbott'),
(46, 'Subramanyam K', 'subramanyam70@yahoo.com', 'Bengaluru ', 'Sjicr ', NULL, NULL, '2021-02-26 20:11:46', '2021-02-26 20:11:46', '2021-02-26 21:42:42', 0, 'Abbott'),
(47, 'LOKESH PRAMOD CHAUDHARI', 'lokeshataipg@rediffmail.com', 'banglore', 'Sjic', NULL, NULL, '2021-02-26 20:12:35', '2021-02-26 20:12:35', '2021-02-26 20:13:35', 0, 'Abbott'),
(48, 'KARUPPIAH', 'sinnakaruppiah@hotmail.com', 'Madural', 'Apollo', NULL, NULL, '2021-02-26 20:14:06', '2021-02-26 20:14:06', '2021-02-26 20:20:37', 0, 'Abbott'),
(49, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Bangalore Baptist hospital', NULL, NULL, '2021-02-26 20:15:55', '2021-02-26 20:15:55', '2021-02-26 21:08:26', 0, 'Abbott'),
(50, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021-02-26 20:16:48', '2021-02-26 20:16:48', '2021-02-26 22:02:30', 0, 'Abbott'),
(51, 'Ajith', 'ajith.jacob@abbott.com', 'Cochin', 'AV', NULL, NULL, '2021-02-26 20:17:41', '2021-02-26 20:17:41', '2021-02-26 20:55:45', 0, 'Abbott'),
(52, 'Mahendra Kumar G', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-02-26 20:17:44', '2021-02-26 20:17:44', '2021-02-26 21:47:44', 0, 'Abbott'),
(53, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-02-26 20:18:06', '2021-02-26 20:18:06', '2021-02-26 21:47:58', 0, 'Abbott'),
(54, 'Francis', 'francisbabul.2007@rediffmail.com', 'Bangalore', 'Mallya hospital', NULL, NULL, '2021-02-26 20:18:35', '2021-02-26 20:18:35', '2021-02-26 20:57:39', 0, 'Abbott'),
(55, 'Sujesh V S', 'sujesh.vs@abbott.com', 'Cochin', 'Abbott', NULL, NULL, '2021-02-26 20:18:43', '2021-02-26 20:18:43', '2021-02-26 20:22:43', 0, 'Abbott'),
(56, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-02-26 20:18:44', '2021-02-26 20:18:44', '2021-03-01 16:03:42', 0, 'Abbott'),
(57, 'Dr Pradeep Kumar K ', 'doctorpradeep@gmail.com', 'Bengaluru/Bangalore', 'Sapthagiri hospitals ', NULL, NULL, '2021-02-26 20:19:58', '2021-02-26 20:19:58', '2021-02-26 22:04:13', 0, 'Abbott'),
(58, 'Sujesh V S', 'sujesh.vs@abbott.com', 'Cochin', 'Abbott', NULL, NULL, '2021-02-26 20:20:42', '2021-02-26 20:20:42', '2021-02-26 20:22:43', 0, 'Abbott'),
(59, 'Shyam', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-02-26 20:20:53', '2021-02-26 20:20:53', '2021-02-26 21:11:02', 0, 'Abbott'),
(60, 'Varun Marimuthu', 'varun.marimuthu@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-02-26 20:22:01', '2021-02-26 20:22:01', '2021-02-26 20:34:59', 0, 'Abbott'),
(61, 'Dr Sunil Kumar S', 'sunilbmc98@gmail.com', 'Bangalore', 'Columbia Asia Hospital', NULL, NULL, '2021-02-26 20:22:11', '2021-02-26 20:22:11', '2021-02-26 21:38:46', 0, 'Abbott'),
(62, 'LAXMI ', 'drlaxmihsn@gmail.com', 'Bengaluru ', 'Jayadeva ', NULL, NULL, '2021-02-26 20:23:00', '2021-02-26 20:23:00', '2021-02-26 20:30:03', 0, 'Abbott'),
(63, 'Varun Marimuthu', 'varun.marimuthu@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-02-26 20:23:25', '2021-02-26 20:23:25', '2021-02-26 20:34:59', 0, 'Abbott'),
(64, 'Ashwin Daware', 'ashwin.daware@skymedical.in', 'BANGALORE', 'Sky medical corporation', NULL, NULL, '2021-02-26 20:23:58', '2021-02-26 20:23:58', '2021-02-26 21:14:34', 0, 'Abbott'),
(65, 'Rahul kolhekar ', 'rahul.vkolhekar@abbott.com', 'Kolkata ', 'AV', NULL, NULL, '2021-02-26 20:24:41', '2021-02-26 20:24:41', '2021-02-26 21:13:25', 0, 'Abbott'),
(66, 'Chris Avineet Joseph', 'Chris.joseph300@gmail.com', 'Bangalore ', 'Manipal hospital ', NULL, NULL, '2021-02-26 20:24:45', '2021-02-26 20:24:45', '2021-02-26 21:48:42', 0, 'Abbott'),
(67, 'Anil Chopra', 'anil.chopra@abbott.com', 'Hyderabad', 'AV', NULL, NULL, '2021-02-26 20:25:15', '2021-02-26 20:25:15', '2021-02-26 21:51:26', 0, 'Abbott'),
(68, 'Dr Debdutta ', 'deb.heartlink@yahoo.com', 'Kolkata ', 'Nh', NULL, NULL, '2021-02-26 20:25:30', '2021-02-26 20:25:30', '2021-02-26 20:33:58', 0, 'Abbott'),
(69, 'Pradeep Kumar K', 'doctorpradeep@gmail.com', 'Bengaluru/Bangalore', 'Sapthagiri hospitals ', NULL, NULL, '2021-02-26 20:25:50', '2021-02-26 20:25:50', '2021-02-26 22:04:13', 0, 'Abbott'),
(70, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-02-26 20:26:08', '2021-02-26 20:26:08', '2021-02-26 21:18:09', 0, 'Abbott'),
(71, 'Dr sunil Christopher ', 'drsunilchristophert@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-02-26 20:26:30', '2021-02-26 20:26:30', '2021-02-26 20:31:24', 0, 'Abbott'),
(72, 'Atanu Banerjee', 'atanu.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-02-26 20:27:45', '2021-02-26 20:27:45', '2021-02-26 20:48:31', 0, 'Abbott'),
(73, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Baptist hospital', NULL, NULL, '2021-02-26 20:28:17', '2021-02-26 20:28:17', '2021-02-26 21:08:26', 0, 'Abbott'),
(74, 'Mohan', 'drmohankhn@gmail.com', 'Bangalore ', 'Cahsr', NULL, NULL, '2021-02-26 20:29:00', '2021-02-26 20:29:00', '2021-02-26 20:35:31', 0, 'Abbott'),
(75, 'Vijay Shah', 'drvtshah19@icloud.com', 'Mumbai', 'Dr v tshah ', NULL, NULL, '2021-02-26 20:30:06', '2021-02-26 20:30:06', '2021-02-26 21:44:07', 0, 'Abbott'),
(76, 'Ravi Shankar', 'ravishakar6211.rr@gmail.com', 'Bangalore', 'Manipal Hospital', NULL, NULL, '2021-02-26 20:30:58', '2021-02-26 20:30:58', '2021-02-26 20:35:20', 0, 'Abbott'),
(77, 'Kalyan ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV', NULL, NULL, '2021-02-26 20:31:13', '2021-02-26 20:31:13', '2021-03-01 16:03:42', 0, 'Abbott'),
(78, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:33:35', '2021-02-26 20:33:35', '2021-02-26 21:12:09', 0, 'Abbott'),
(79, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:33:42', '2021-02-26 20:33:42', '2021-02-26 21:12:09', 0, 'Abbott'),
(80, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:33:51', '2021-02-26 20:33:51', '2021-02-26 21:12:09', 0, 'Abbott'),
(81, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:33:58', '2021-02-26 20:33:58', '2021-02-26 21:12:09', 0, 'Abbott'),
(82, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:34:02', '2021-02-26 20:34:02', '2021-02-26 21:12:09', 0, 'Abbott'),
(83, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:34:02', '2021-02-26 20:34:02', '2021-02-26 21:12:09', 0, 'Abbott'),
(84, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:34:06', '2021-02-26 20:34:06', '2021-02-26 21:12:09', 0, 'Abbott'),
(85, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:34:06', '2021-02-26 20:34:06', '2021-02-26 21:12:09', 0, 'Abbott'),
(86, 'Disha', 'dishaganeshshetty@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-02-26 20:34:47', '2021-02-26 20:34:47', '2021-02-26 20:41:17', 0, 'Abbott'),
(87, 'Sunil gouniyal', 'drsunilgouniyal@yahoo.co.in', 'Raipur ', 'NH', NULL, NULL, '2021-02-26 20:34:51', '2021-02-26 20:34:51', '2021-02-26 20:40:06', 0, 'Abbott'),
(88, 'Priyabrata sahu', 'pspriyabrata@gmail.com', 'Bhubaneswar', 'Apollo', NULL, NULL, '2021-02-26 20:35:51', '2021-02-26 20:35:51', '2021-02-26 20:37:21', 0, 'Abbott'),
(89, 'Ravi', 'ravishankar6211.rr@gmail.com', 'Bangalore', 'Manipal Hospital', NULL, NULL, '2021-02-26 20:37:12', '2021-02-26 20:37:12', '2021-02-26 20:41:16', 0, 'Abbott'),
(90, 'Raguraman', 'raguraman.sundar@gmail.com', 'Bangalore', 'Jayadeva institute', NULL, NULL, '2021-02-26 20:37:22', '2021-02-26 20:37:22', '2021-02-26 21:21:39', 0, 'Abbott'),
(91, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:37:29', '2021-02-26 20:37:29', '2021-02-26 21:12:09', 0, 'Abbott'),
(92, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:37:41', '2021-02-26 20:37:41', '2021-02-26 21:12:09', 0, 'Abbott'),
(93, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:37:41', '2021-02-26 20:37:41', '2021-02-26 21:12:09', 0, 'Abbott'),
(94, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:38:14', '2021-02-26 20:38:14', '2021-02-26 21:12:09', 0, 'Abbott'),
(95, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:38:14', '2021-02-26 20:38:14', '2021-02-26 21:12:09', 0, 'Abbott'),
(96, 'Sawan Roy', 'sawan.roy@abbott.com', 'Hyderabad', 'AV', NULL, NULL, '2021-02-26 20:38:20', '2021-02-26 20:38:20', '2021-02-26 21:47:49', 0, 'Abbott'),
(97, 'Anil Chopra', 'anil.chopra@abbott.com', 'Hyderabad', 'Av', NULL, NULL, '2021-02-26 20:38:24', '2021-02-26 20:38:24', '2021-02-26 21:51:26', 0, 'Abbott'),
(98, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital ', NULL, NULL, '2021-02-26 20:38:28', '2021-02-26 20:38:28', '2021-02-26 21:12:09', 0, 'Abbott'),
(99, 'Raguraman', 'raguraman.sundar@gmail.com', 'Bangalore', 'Jayadeva institute', NULL, NULL, '2021-02-26 20:39:29', '2021-02-26 20:39:29', '2021-02-26 21:21:39', 0, 'Abbott'),
(100, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-02-26 20:39:35', '2021-02-26 20:39:35', '2021-02-26 21:47:33', 0, 'Abbott'),
(101, 'Vinodkumar', 'vinodgkumar25@gmail.com', 'Bengaluru', 'Manipal hospital', NULL, NULL, '2021-02-26 20:40:03', '2021-02-26 20:40:03', '2021-02-26 21:48:39', 0, 'Abbott'),
(102, 'Satyaranjan Mohanty ', 'satya.bapun@gmail.com', 'Bhubaneswar ', 'Apollo ', NULL, NULL, '2021-02-26 20:40:05', '2021-02-26 20:40:05', '2021-02-26 22:01:15', 0, 'Abbott'),
(103, 'ASHA K S', 'ashasaju98@gmail.com', 'Wayanad', 'Lourde hospital', NULL, NULL, '2021-02-26 20:40:56', '2021-02-26 20:40:56', '2021-02-26 20:51:49', 0, 'Abbott'),
(104, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital Udupi ', NULL, NULL, '2021-02-26 20:41:41', '2021-02-26 20:41:41', '2021-02-26 21:12:09', 0, 'Abbott'),
(105, 'Ashwin Daware', 'ashwin.daware@skymedical.in', 'BANGALORE', 'Sky medical corporation', NULL, NULL, '2021-02-26 20:41:50', '2021-02-26 20:41:50', '2021-02-26 21:14:34', 0, 'Abbott'),
(106, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital Udupi ', NULL, NULL, '2021-02-26 20:41:55', '2021-02-26 20:41:55', '2021-02-26 21:12:09', 0, 'Abbott'),
(107, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital Udupi ', NULL, NULL, '2021-02-26 20:41:59', '2021-02-26 20:41:59', '2021-02-26 21:12:09', 0, 'Abbott'),
(108, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital Udupi ', NULL, NULL, '2021-02-26 20:42:06', '2021-02-26 20:42:06', '2021-02-26 21:12:09', 0, 'Abbott'),
(109, 'Sadananda ', 'sadanandaasp@yahoo.com', 'Udupi ', 'Adarsha Hospital Udupi ', NULL, NULL, '2021-02-26 20:42:30', '2021-02-26 20:42:30', '2021-02-26 21:12:09', 0, 'Abbott'),
(110, 'Balaraj', 'balarajud2000@gmail.com', 'Bangalore ', 'Jayadeva', NULL, NULL, '2021-02-26 20:45:00', '2021-02-26 20:45:00', '2021-02-26 21:11:08', 0, 'Abbott'),
(111, 'Dr Debasis Ghosh', 'Ghosh.d@gmail.com', 'Kolkata ', 'Apollo', NULL, NULL, '2021-02-26 20:48:38', '2021-02-26 20:48:38', '2021-02-26 20:49:38', 0, 'Abbott'),
(112, 'Bino', 'binobenjaminc@gmail.com', 'Thrissur ', 'JMMC', NULL, NULL, '2021-02-26 20:48:58', '2021-02-26 20:48:58', '2021-02-26 21:11:56', 0, 'Abbott'),
(113, 'Ravi', 'ravishankar6211.rr@gmail.com', 'Bangalore', 'Manipal Hospital', NULL, NULL, '2021-02-26 20:49:09', '2021-02-26 20:49:09', '2021-02-26 20:49:39', 0, 'Abbott'),
(114, 'Dr Swapan Kumar De', 'dey.swapan@gamil.com', 'Kol', 'Apollo', NULL, NULL, '2021-02-26 20:49:22', '2021-02-26 20:49:22', '2021-02-26 20:50:39', 0, 'Abbott'),
(115, 'Praveen', 'pjshetty1974@rediffmail.com', 'Mangalore', 'A J Hospital', NULL, NULL, '2021-02-26 20:49:53', '2021-02-26 20:49:53', '2021-02-26 21:44:53', 0, 'Abbott'),
(116, 'BINO', 'binobenjaminc@gmail.com', 'thrissur', 'JMMC', NULL, NULL, '2021-02-26 20:50:04', '2021-02-26 20:50:04', '2021-02-26 21:11:56', 0, 'Abbott'),
(117, 'Dr Sunip ', 'sunip.banerjee@yahoo.com', 'Kol', 'S G ', NULL, NULL, '2021-02-26 20:50:12', '2021-02-26 20:50:12', '2021-02-26 20:51:13', 0, 'Abbott'),
(118, 'Dr M Lodha', 'lodha.m42@gmail.com', 'Kolkata  ', 'RKM', NULL, NULL, '2021-02-26 20:50:52', '2021-02-26 20:50:52', '2021-02-26 20:52:01', 0, 'Abbott'),
(119, 'Dr Bikash Majumder ', 'majumder.b@gmail.com', 'Kolkata', 'Apollo G ', NULL, NULL, '2021-02-26 20:51:37', '2021-02-26 20:51:37', '2021-02-26 20:52:54', 0, 'Abbott'),
(120, 'Ravi', 'ravishankar6211.rr@gmail.com', 'Bangalore', 'Manipal Hospital', NULL, NULL, '2021-02-26 20:51:48', '2021-02-26 20:51:48', '2021-02-26 20:52:18', 0, 'Abbott'),
(121, 'NARAYANEE RAJASEKARAN', 'narayaneerajasekaran@yahoo.com', 'Bangalore', 'Sri Jayadeva Institute ', NULL, NULL, '2021-02-26 20:52:22', '2021-02-26 20:52:22', '2021-02-26 21:02:53', 0, 'Abbott'),
(122, 'Dr Raja Nag ', 'dr.raja.nag@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-02-26 20:52:27', '2021-02-26 20:52:27', '2021-02-26 22:09:21', 0, 'Abbott'),
(123, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-02-26 20:57:20', '2021-02-26 20:57:20', '2021-02-26 21:11:02', 0, 'Abbott'),
(124, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-02-26 21:03:00', '2021-02-26 21:03:00', '2021-02-26 21:11:02', 0, 'Abbott'),
(125, 'Snehal Paul', 'paul_snehal@yahoo.com', 'Bangalore', 'Fortis hospital Cunningham road Bangalore', NULL, NULL, '2021-02-26 21:09:17', '2021-02-26 21:09:17', '2021-02-26 21:11:19', 0, 'Abbott'),
(126, 'Sudhakar Rao', 'msudhakar88@gmail.com', 'Manipal', 'Kmc', NULL, NULL, '2021-02-26 21:12:42', '2021-02-26 21:12:42', '2021-02-26 21:19:12', 0, 'Abbott'),
(127, 'Satyaranjan Mohanty ', 'satya.bapun@gmail.com', 'Bhubaneswar ', 'Apollo ', NULL, NULL, '2021-02-26 21:13:44', '2021-02-26 21:13:44', '2021-02-26 22:01:15', 0, 'Abbott'),
(128, 'Kuntal Bhattacharyya ', 'drkuntal@gmail.com', 'Kolkata ', 'Rti', NULL, NULL, '2021-02-26 21:15:31', '2021-02-26 21:15:31', '2021-02-26 21:27:36', 0, 'Abbott'),
(129, 'Mahendra Kumar G', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-02-26 21:17:46', '2021-02-26 21:17:46', '2021-02-26 21:47:44', 0, 'Abbott'),
(130, 'Pradeep Kumar K', 'doctorpradeep@gmail.com', 'Bengaluru/Bangalore', 'Sapthagiri hospitals ', NULL, NULL, '2021-02-26 21:17:47', '2021-02-26 21:17:47', '2021-02-26 22:04:13', 0, 'Abbott'),
(131, 'Satyaranjan Mohanty ', 'satya.bapun@gmail.com', 'Bhubaneswar ', 'Apollo ', NULL, NULL, '2021-02-26 21:18:30', '2021-02-26 21:18:30', '2021-02-26 22:01:15', 0, 'Abbott'),
(132, 'Shaik', 'aaalanoor@gmail.com', 'Hyderabad', 'Apolo', NULL, NULL, '2021-02-26 21:33:21', '2021-02-26 21:33:21', '2021-03-01 23:39:03', 0, 'Abbott'),
(133, 'Anil Chopra', 'anil.chopra@abbott.com', 'Hyd', 'AV', NULL, NULL, '2021-02-26 21:40:18', '2021-02-26 21:40:18', '2021-02-26 21:51:26', 0, 'Abbott'),
(134, 'Mahendra Kumar G', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-02-26 21:42:13', '2021-02-26 21:42:13', '2021-02-26 21:47:44', 0, 'Abbott'),
(135, 'shaik', 'aaalanoor@gmail.com', 'Hyderabad', 'appolo', NULL, NULL, '2021-02-26 21:44:16', '2021-02-26 21:44:16', '2021-03-01 23:39:03', 0, 'Abbott'),
(136, 'Noor Khan', 'aaalanoor@gmail.com', 'Hyderabad', 'appolo', NULL, NULL, '2021-02-27 00:29:01', '2021-02-27 00:29:01', '2021-03-01 23:39:03', 0, 'Abbott'),
(137, 'Arunkumar ', 'a.rubesh@gmail.com', 'Bangalore ', 'SJICR ', NULL, NULL, '2021-02-27 08:00:18', '2021-02-27 08:00:18', '2021-02-27 08:00:47', 0, 'Abbott'),
(138, 'M W APP ', 'mw.app81@gmail.com', 'Mumbai ', 'M W HOSPITAL ', NULL, NULL, '2021-02-27 16:50:53', '2021-02-27 16:50:53', '2021-02-27 16:57:04', 0, 'Abbott'),
(139, 'Noor Khan', 'aaalanoor@gmail.com', 'Hyderabad', 'Apolo', NULL, NULL, '2021-02-27 18:43:37', '2021-02-27 18:43:37', '2021-03-01 23:39:03', 0, 'Abbott'),
(140, 'Dr Kanhai Lalani', 'lalanirc@gmail.com', 'Manipal', 'KASTURBA MEDICAL COLLEGE, MANIPAL', NULL, NULL, '2021-02-27 20:17:31', '2021-02-27 20:17:31', '2021-02-27 20:18:01', 0, 'Abbott'),
(141, 'Dr Ashalatha', 'ashalathab48@gmail.com', 'Bangalore', 'Saptagiri institute of medical sciences and research', NULL, NULL, '2021-02-27 20:23:44', '2021-02-27 20:23:44', '2021-02-27 20:24:14', 0, 'Abbott'),
(142, 'Noor Khan', 'aaalanoor@gmail.com', 'Hyderabad', 'appolo', NULL, NULL, '2021-02-28 13:45:24', '2021-02-28 13:45:24', '2021-03-01 23:39:03', 0, 'Abbott'),
(143, 'Kalyan', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'Av', NULL, NULL, '2021-03-01 15:59:09', '2021-03-01 15:59:09', '2021-03-01 16:03:42', 0, 'Abbott'),
(144, 'Noor Khan', 'aaalanoor@gmail.com', 'Hyderabad', 'appolo', NULL, NULL, '2021-03-01 23:38:03', '2021-03-01 23:38:03', '2021-03-01 23:39:03', 0, 'Abbott'),
(145, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'scefdw', NULL, NULL, '2021-03-02 13:00:01', '2021-03-02 13:00:01', '2021-03-02 13:15:08', 0, 'Abbott');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
